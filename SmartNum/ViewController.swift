//
//  ViewController.swift
//  SmartNum
//
//  Created by octagon studio on 15/09/18.
//  Copyright © 2018 Cordova. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController {
    @IBOutlet weak var DrawView: DrawView!
    @IBOutlet weak var predictLabel: UILabel!
    
    lazy var classificationRequest: VNCoreMLRequest = {
        // Load the ML model through its generated class and create a Vision request for it.
        do {
            let model = try VNCoreMLModel(for: MNIST().model)
            return VNCoreMLRequest(model: model, completionHandler: self.handleClassification)
        } catch {
            fatalError("Can't load Vision ML model: \(error).")
        }
    }()
    
    func handleClassification(request: VNRequest, error: Error?) {
        guard let observations = request.results as? [VNClassificationObservation]
            else { fatalError("Unexpected result type from VNCoreMLRequest.") }
        guard let best = observations.first
            else { fatalError("Can't get best result.") }
        
        DispatchQueue.main.async {
            self.predictLabel.text = best.identifier
            self.predictLabel.isHidden = false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        predictLabel.isHidden = true
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        
        DrawView.lines = []
        DrawView.setNeedsDisplay()
        predictLabel.isHidden = true
        
        
    }
    
    @IBAction func predictTapped(_ sender: UIButton) {
        
        
        guard let context = DrawView.getViewContext(),
            let inputImage = context.makeImage()
            else { fatalError("Get context or make image failed.") }
        // DONE: Perform request on model
        let ciImage = CIImage(cgImage: inputImage)
        let handler = VNImageRequestHandler(ciImage: ciImage)
        do {
            try handler.perform([classificationRequest])
        } catch {
            print(error)
        }

        
    }
    

    
}

